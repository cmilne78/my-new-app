import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
<div>
    <div className="Hero">
        <div className="HeroGroup">
        <h1>Better living through better thinking</h1>
    <p>I'm going to use this a place to learn and to think.</p>
    <Link to="/page-2/">Go to page two</Link><br />
    <Link to="/video">Watch the video</Link>
        </div>
    </div>
    
  </div>
)

export default SecondPage
