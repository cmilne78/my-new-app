import React from 'react'
import Link from 'gatsby-link'
import './Header.css'

// components/header.js


class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      hasScrolled: false
    }
  }
  componentDidMount() {
    window.addEventListener('scroll',
    this.handleScroll)
  }

  handleScroll = (event) => {
    const scrollTop = window.pageYOffset

    if (scrollTop > 50) {
      this.setState({ hasScrolled: true})
    } else {
      this.setState({ hasScrolled: false})
    }
  }
  render() {
    return (
    <div className={this.state.hasScrolled ?
    'Header HeaderScrolled' : 'Header'}>
      <div className="HeaderGroup">
        <Link to="/"><img width="36" src={require('../images/logo.svg')} /></Link>
        <Link to="/page-2">Guitar</Link>
        <Link to="/page-3">Exercise</Link>
        <Link to="/workshops">Code</Link>
      </div>
    </div>
      )
  }
}
export default Header
