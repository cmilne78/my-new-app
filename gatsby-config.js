module.exports = {
  siteMetadata: {
    title: 'Christopher Milne',
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
